(ns dbservice.handler
  (:use compojure.core)
  (:require [compojure.handler :as handler]
            [compojure.route :as route]
            [dbservice.jdbc :as db]
            [ring.middleware.json :as json]
            [ring.util.response :as ring]
            ))

(defroutes app-routes
  (GET "/" [] "Hello World")
  (GET "/T3" [] "Hello T3")
  (GET "/jdbc" []
       ;; (db/first-query)
       (ring/response
        (db/first-query)
        )
       )
  (route/resources "/")
  (route/not-found "Not Found"))

(def app
  (json/wrap-json-response (handler/site #'app-routes)))
