(ns dbservice.jdbc
  (:require [clojure.java.jdbc :as jdbc])
  )

(def devdb01
  {:classname   "oracle.jdbc.driver.OracleDriver"
   :subprotocol "oracle:thin"
   :user        "lwang"
   :password    "pwd"
   :subname     "@10.6.124.225:1521/devdb01"})

(defn first-query []
  (clojure.java.jdbc/with-connection devdb01
    (clojure.java.jdbc/with-query-results rows
      ["select * from lwang_demo.form_data where rownum < 10"]
      ;; (first rows)
      ;; (doall (map fn rows))
            (->> rows
           (first)
           (filter #(not (= oracle.sql.BLOB (type (val %)))))
           (into {}))

      )))

(defn pm1 [map]
  (get map :form_data_id))

(defn pm2 [map]
  (get map :form_data_status))




