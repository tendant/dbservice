(defproject dbservice "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [compojure "1.1.5"]
                 [com.oracle/ojdbc6 "11.2.0.2.0-11814893"]
                 [org.clojure/java.jdbc "0.2.3"]
                 [ring/ring-json "0.2.0"]
                ]
  :plugins [[lein-ring "0.8.2"]]
  :ring {:handler dbservice.handler/app}
  :profiles
  {:dev {:dependencies [[ring-mock "0.1.3"]]}}
  :repositories {"sf"
                 "http://ivy/artifactory/repo"}
)
